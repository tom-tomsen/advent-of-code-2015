<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$ref = [
    'children' => 3,
    'cats' => 7,
    'samoyeds' => 2,
    'pomeranians' => 3,
    'akitas' => 0,
    'vizslas' => 0,
    'goldfish' => 5,
    'trees' => 3,
    'cars' => 2,
    'perfumes' => 1,
];

$lines = \explode(\PHP_EOL, \trim($fileContent));

$total = 0;

foreach ($lines as $line) {
    [$sueId, $anim1, $count1, $anim2, $count2, $anim3, $count3]
        = \sscanf($line, 'Sue %d: %[a-zA-Z]: %d, %[a-zA-Z]: %d, %[a-zA-Z]: %d');

    if (103 === $sueId) {
        continue;
    }

    $animals = [
        $anim1 => $count1,
        $anim2 => $count2,
        $anim3 => $count3,
    ];

    $found = 0;

    foreach ($animals as $anim => $count) {
        if (\in_array($anim, ['cats', 'trees'], true) && $ref[$anim] < $count
        || \in_array($anim, ['pomeranians', 'goldfish'], true) && $ref[$anim] > $count
        || $ref[$anim] === $count
        ) {
            ++$found;
        }
    }

    if (3 === $found) {
        answer($sueId);
    }
}
