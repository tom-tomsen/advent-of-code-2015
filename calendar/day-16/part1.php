<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$ref = [
    'children' => 3,
    'cats' => 7,
    'samoyeds' => 2,
    'pomeranians' => 3,
    'akitas' => 0,
    'vizslas' => 0,
    'goldfish' => 5,
    'trees' => 3,
    'cars' => 2,
    'perfumes' => 1,
];

$lines = \explode(\PHP_EOL, \trim($fileContent));

$total = 0;

foreach ($lines as $line) {
    [$sueId, $anim1, $count1, $anim2, $count2, $anim3, $count3]
        = \sscanf($line, 'Sue %d: %[a-zA-Z]: %d, %[a-zA-Z]: %d, %[a-zA-Z]: %d');

    if (\array_key_exists($anim1, $ref) && $count1 === $ref[$anim1]
    && \array_key_exists($anim2, $ref) && $count2 === $ref[$anim2]
    && \array_key_exists($anim3, $ref) && $count3 === $ref[$anim3]
    ) {
        answer($sueId);
    }
}
