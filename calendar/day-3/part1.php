<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$posX = 0;
$posY = 0;
$visits = [
    '0-0' => 1,
];
$total = 1;

for ($i = 0; \mb_strlen($fileContent) > $i; ++$i) {
    $char = $fileContent[$i];

    switch ($char) {
        case '^':
            $posY--;

        break;
        case 'v':
            $posY++;

        break;
        case '>':
            $posX++;

        break;
        case '<':
            $posX--;

        break;
    }

    $key = $posX . '-' . $posY;

    if (!\array_key_exists($key, $visits)) {
        $visits[$key] = 1;
        $total += $visits[$key];
    }
}

answer($total);
