<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$posXSanta = 0;
$posYSanta = 0;
$posXRobot = 0;
$posYRobot = 0;
$visits = [
    '0-0' => 1,
];
$total = 1;

for ($i = 0; \mb_strlen($fileContent) > $i; ++$i) {
    $char = $fileContent[$i];

    if (0 === $i % 2) {
        switch ($char) {
            case '^':
                $posYSanta--;

            break;
            case 'v':
                $posYSanta++;

            break;
            case '>':
                $posXSanta++;

            break;
            case '<':
                $posXSanta--;

            break;
        }
    } else {
        switch ($char) {
            case '^':
                $posYRobot--;

            break;
            case 'v':
                $posYRobot++;

            break;
            case '>':
                $posXRobot++;

            break;
            case '<':
                $posXRobot--;

            break;
        }
    }

    $key = $posXSanta . '-' . $posYSanta;

    if (!\array_key_exists($key, $visits)) {
        $visits[$key] = 1;
        $total += $visits[$key];
    }

    $key = $posXRobot . '-' . $posYRobot;

    if (!\array_key_exists($key, $visits)) {
        $visits[$key] = 1;
        $total += $visits[$key];
    }
}

answer($total);
