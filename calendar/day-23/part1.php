<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$instructions = [];

foreach ($lines as $line) {
    $instructions[] = \explode(' ', \str_replace(',', '', $line));
}

$a = 0;
$b = 0;
$i = -1;

while (\count($instructions) > ++$i) {
    switch ($instructions[$i][0]) {
        case 'hlf':
            ${$instructions[$i][1]} /= 2;

        break;
        case 'tpl':
            ${$instructions[$i][1]} *= 3;

        break;
        case 'inc':
            ${$instructions[$i][1]}++;

        break;
        case 'jmp':
            $i += $instructions[$i][1] - 1;

        break;
        case 'jie':
            if (0 === ${$instructions[$i][1]} % 2) {
                $i += $instructions[$i][2] - 1;
            }

        break;
        case 'jio':
            if (1 === ${$instructions[$i][1]}) {
                $i += $instructions[$i][2] - 1;
            }

        break;
    }
}

answer($b);
