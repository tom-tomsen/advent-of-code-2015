<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

use drupol\phpermutations\Generators\Permutations;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$total = 0;
$participants = [];

foreach ($lines as $line) {
    [$playerA, $feeling, $count, $playerB] =
        \sscanf($line, '%s would %s %d happiness units by sitting next to %[^.].');

    $participants[$playerA] = $playerA;
    $participants[$playerB] = $playerB;
    $stats[$playerA . '-' . $playerB] = ('lose' === $feeling ? -1 : 1) * $count;
}

$participants['me'] = 'me';

foreach ($participants as $p) {
    $stats['me-' . $p] = 0;
    $stats[$p . '-me'] = 0;
}

$permutations = new Permutations($participants, \count($participants));

$max = 0;

foreach ($permutations->generator() as $p) {
    $count = calcCount($p, $stats);

    if ($count > $max) {
        $max = $count;
    }
}

answer($max);

function calcCount($p, $s)
{
    $n = 0;
    \array_unshift($p, $p[\count($p) - 1]);
    \array_push($p, $p[1]);

    for ($i = 1; \count($p) - 1 > $i; ++$i) {
        $x = $p[$i];
        $n += $s[$x . '-' . $p[$i - 1]];
        $n += $s[$x . '-' . $p[$i + 1]];
    }

    return $n;
}
