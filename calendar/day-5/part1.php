<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$lines = \explode(\PHP_EOL, $fileContent);

$nice = 0;

foreach ($lines as $line) {
    $line = \trim($line);

    if (
        3 <= \preg_match_all('~[aeiou]~', $line) &&
        0 < \preg_match('~(.)\1~', $line) &&
        0 === \preg_match('~ab|cd|pq|xy~', $line)
    ) {
        ++$nice;
    }
}
answer($nice);
