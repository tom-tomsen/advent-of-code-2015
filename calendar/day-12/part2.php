<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$object = \json_decode(\trim($fileContent), false);
$sum = sum($object);

function sum($arr)
{
    $sum = 0;

    if (\is_array($arr)) {
        foreach ($arr as $elem) {
            $sum += sum($elem);
        }
    } elseif (\is_object($arr)) {
        $arr = (array) $arr;
        $tmpSum = 0;

        foreach ($arr as $elem) {
            if ('red' === $elem) {
                $tmpSum = 0;

                break;
            }
            $tmpSum += sum($elem);
        }

        $sum += $tmpSum;
    } elseif (\is_numeric($arr)) {
        $sum += $arr;
    } elseif (\is_string($arr)) {
        if ('red' === $arr) {
        }
        // nothing
    } else {
        \var_dump($arr);

        die;
    }

    return $sum;
}

answer($sum);
