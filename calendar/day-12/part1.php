<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$matches = [];
\preg_match_all('~-?[0-9]+~', $fileContent, $matches);

$sum = 0;

foreach ($matches[0] as $number) {
    $sum += $number;
}

answer($sum);
