<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$goal = ((int) $fileContent) / 10;

do {
    $sum = 0;
    $sqrt = \sqrt(++$i);

    for ($x = 1; $x <= $sqrt; ++$x) {
        if (!($i % $x)) {
            $sum += ($x !== $sqrt) ? $i / $x + $x : $x;
        }
    }
} while ($sum < $goal);

answer($i);
