<?php

declare(strict_types=1);

// https://www.reddit.com/r/adventofcode/comments/3xflz8/day_19_solutions/cy4ctw3/

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$x = '';
$r = [];
$i = 0;

foreach ($lines as $line) {
    if ('' === \trim($line)) {
        ++$i;

        continue;
    }

    if (0 === $i) {
        [$key, $value] = \sscanf($line, '%s => %s');
        $r[] = [$key, $value];
    } else {
        $x = $line;
    }
}

$i = 0;

while ('e' !== $x) {
    foreach ($r as $transition) {
        $key = $transition[0];
        $value = $transition[1];

        while (false !== ($pos = \mb_strpos($x, $value))) {
            $x = \substr_replace($x, $key, $pos, \mb_strlen($value));
            ++$i;
        }
    }
}

answer($i);
