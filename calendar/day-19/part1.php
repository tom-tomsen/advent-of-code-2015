<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$x = '';
$r = [];
$i = 0;

foreach ($lines as $line) {
    if ('' === \trim($line)) {
        ++$i;

        continue;
    }

    if (0 === $i) {
        [$key, $value] = \sscanf($line, '%s => %s');
        $r[] = [$key, $value];
    } else {
        $x = $line;
    }
}

$map = [];

foreach ($r as $transition) {
    $key = $transition[0];
    $value = $transition[1];

    $offset = 0;

    while (false !== ($found = \mb_strpos($x, $key, $offset))) {
        $v = \mb_substr($x, 0, $found) . $value . \mb_substr($x, $found + \mb_strlen($key));
        $map[$v] = true;
        $offset = $found + 1;
    }
}

answer(\count($map));
