<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

use drupol\phpermutations\Generators\Combinations;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$weapons = [
    ['Dagger', 8, 4, 0],
    ['Shortsword', 10, 5, 0],
    ['Warhammer', 25, 6, 0],
    ['Longsword', 40, 7, 0],
    ['Greataxe', 74, 8, 0],
];

$armors = [
    ['Bare', 0, 0, 0],
    ['Leather', 13, 0, 1],
    ['Chainmail', 31, 0, 2],
    ['Splintmail', 53, 0, 3],
    ['Bandedmail', 75, 0, 4],
    ['Platemail', 102, 0, 5],
];

$rings = [
    ['Bare', 0, 0, 0],
    ['Bare', 0, 0, 0],
    ['Damage +1', 25, 1, 0],
    ['Damage +2', 50, 2, 0],
    ['Damage +3', 100, 3, 0],
    ['Defense +1', 20, 0, 1],
    ['Defense +2', 40, 0, 2],
    ['Defense +3', 80, 0, 3],
];

$max = 0;
$equip = [];

$ring_combinations = new Combinations($rings, 2);

foreach ($weapons as $weapon) {
    foreach ($armors as $armor) {
        foreach ($ring_combinations->generator() as $ring) {
            $enemy = [
                'health' => 109,
                'attack' => 8,
                'armor' => 2,
            ];

            $player = [
                'health' => 100,
                'attack' => 0 + $weapon[2] + $armor[2] + $ring[0][2] + $ring[1][2],
                'armor' => 0 + $weapon[3] + $armor[3] + $ring[0][3] + $ring[1][3],
            ];

            fight($player, $enemy);

            if (0 >= $player['health']) {
                $cost = 0;
                $cost += $weapon[1];
                $cost += $armor[1];
                $cost += $ring[0][1];
                $cost += $ring[1][1];

                if ($max < $cost) {
                    $equip = ([
                        'weapon' => $weapon[0],
                        'armor' => $armor[0],
                        'ring1' => $ring[0][0],
                        'ring2' => $ring[1][0],
                    ]);
                    $max = $cost;
                }
                /*
                                var_dump('PLAYER LOST', json_encode([
                                    'weapon' => $weapon[0],
                                    'armor' => $armor[0],
                                    'ring1' => $ring[0][0],
                                    'ring2' => $ring[1][0],
                                ]));
                 */
            }
            /*
                            var_dump(json_encode([
                                'WON' => 'WON',
                                'weapon' => $weapon[0],
                                'armor' => $armor[0],
                                'ring1' => $ring[0][0],
                                'ring2' => $ring[1][0],
                                'cost' => $cost,
                            ]));
             */
        }
    }
}

answer($max);

function fight(&$player, &$enemy) : void
{
    while (0 < $enemy['health']) {
        $dmg = dmg($player, $enemy);
        $enemy['health'] -= $dmg;
        $dmg = dmg($enemy, $player);
        $player['health'] -= $dmg;
    }
}

function dmg($char, $enemy)
{
    return \max(1, $char['attack'] - $enemy['armor']);
}
