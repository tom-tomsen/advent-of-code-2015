<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

use drupol\phpermutations\Generators\Permutations;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$routes = [];
$dest = [];

foreach ($lines as $line) {
    [$from, $to, $distance] = \sscanf($line, '%s to %s = %d');

    $routes[$from . '-' . $to] = $distance;
    $routes[$to . '-' . $from] = $distance;
    $dest[$from] = $from;
    $dest[$to] = $to;
}

$Permutations = new Permutations($dest);

$min = \PHP_INT_MAX;

foreach ($Permutations->generator() as $path) {
    $distance = 0;

    for ($i = 1; \count($path) > $i; ++$i) {
        $start = $path[$i - 1];
        $end = $path[$i];

        if (!\array_key_exists("{$start}-{$end}", $routes)) {
            $distance = 0;

            break;
        }

        $distance += $routes["{$start}-{$end}"];
    }

    if (0 < $distance) {
        $min = \min($min, $distance);
    }
}

answer($min);
