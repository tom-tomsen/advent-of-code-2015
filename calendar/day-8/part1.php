<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$total = 0;

foreach (\file(__DIR__ . '/input.txt', \FILE_IGNORE_NEW_LINES) as $line) {
    eval('$str = ' . $line . ';');
    $total += \mb_strlen($line) - \mb_strlen($str);
}

answer($total);
