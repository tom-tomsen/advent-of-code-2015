<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

use drupol\phpermutations\Generators\Combinations;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$lines = \explode(\PHP_EOL, \trim($fileContent));

$containers = [];

foreach ($lines as $line) {
    $containers[] = $line;
}

$count = [];
$min = \count($containers);

for ($i = 1; \count($containers) >= $i; ++$i) {
    $combinations = new Combinations($containers, $i);

    foreach ($combinations->generator() as $p) {
        $sum = \array_sum($p);

        if (150 === $sum) {
            $c = \count($p);
            $min = \min($c, $min);

            if (\array_key_exists($c, $count)) {
                ++$count[$c];
            } else {
                $count[$c] = 1;
            }
        }
    }
}

answer($count[$min]);
