<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

use drupol\phpermutations\Generators\Combinations;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$lines = \explode(\PHP_EOL, \trim($fileContent));

$containers = [];

foreach ($lines as $line) {
    $containers[] = $line;
}

$count = 0;

for ($i = 1; \count($containers) >= $i; ++$i) {
    $combinations = new Combinations($containers, $i);

    foreach ($combinations->generator() as $p) {
        $sum = \array_sum($p);

        if (150 === $sum) {
            ++$count;
        }
    }
}

answer($count);
