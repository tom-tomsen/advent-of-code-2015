<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$total = 0;
$grid = [];

foreach ($lines as $i => $line) {
    $grid[$i] = [];

    for ($j = 0; \mb_strlen($line) > $j; ++$j) {
        $grid[$i][$j] = $line[$j];
    }
}

for ($n = 0; 100 > $n; ++$n) {
    $next = [];

    for ($i = 0; \count($grid) > $i; ++$i) {
        $next[] = [];

        for ($j = 0; \count($grid[$i]) > $j; ++$j) {
            $on = 0;
            $off = 0;

            for ($a = -1; 1 >= $a; ++$a ) {
                for ($b = -1; 1 >= $b; ++$b ) {
                    if (0 === $a && 0 === $b) {
                        continue;
                    }

                    if (\array_key_exists($i + $a, $grid)
                    && \array_key_exists($j + $b, $grid[$i + $a])) {
                        if ('#' === $grid[$i + $a][$j + $b]) {
                            ++$on;
                        } else {
                            ++$off;
                        }
                    } else {
                        ++$off;
                    }
                }
            }

            if ('#' === $grid[$i][$j]) {
                if (2 === $on || 3 === $on) {
                    $next[$i][$j] = '#';
                } else {
                    $next[$i][$j] = '.';
                }
            } else {
                if (3 === $on) {
                    $next[$i][$j] = '#';
                } else {
                    $next[$i][$j] = '.';
                }
            }
        }
    }
    $grid = \json_decode(\json_encode($next));
}

$sum = 0;

for ($i = 0; \count($grid) > $i; ++$i) {
    for ($j = 0; \count($grid[$i]) > $j; ++$j) {
        if ('#' === $grid[$i][$j]) {
            ++$sum;
        }
    }
}

answer($sum);

function printGrid(array $grid) : void
{
    foreach ($grid as $row) {
        foreach ($row as $col) {
            echo $col;
        }
        echo \PHP_EOL;
    }
    echo \PHP_EOL;
    echo \PHP_EOL;
}
