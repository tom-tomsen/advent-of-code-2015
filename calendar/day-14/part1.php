<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$deers = [];

foreach ($lines as $line) {
    [$name, $speed, $up, $down] = \sscanf($line, '%s can fly %d km/s for %d seconds, but then must rest for %d seconds.');
    $deers[] = [
        'name' => $name,
        'speed' => $speed,
        'up' => $up,
        'down' => $down,
    ];
}

$max = 0;
$time = 2503;

foreach ($deers as $deer) {
    $total = $deer['up'] + $deer['down'];
    $times = \floor($time / $total);
    $x = $total * $times;
    $remainder = $time - $x;

    if ($remainder > $deer['up']) {
        $add = $deer['up'];
    } else {
        $add = $remainder;
    }

    $distance = $times * $deer['speed'] * $deer['up'] + $add * $deer['speed'];

    $max = \max($max, $distance);
}

answer($max);
/*
14km 10s 127s
16km 11s 162s

1000 / 137 = 7.2
137 * 7 = 959
8 * 14 * 10 = 1120

1000 / 172 = 5.8
172 * 5 = 860
6 * 16 * 11 = 1056
 */
