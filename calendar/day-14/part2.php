<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$deers = [];

foreach ($lines as $line) {
    [$name, $speed, $up, $down] = \sscanf($line, '%s can fly %d km/s for %d seconds, but then must rest for %d seconds.');
    $deers[] = [
        'name' => $name,
        'speed' => $speed,
        'up' => $up,
        'down' => $down,
        'points' => 0,
    ];
}

$time = 2503;

for ($second = 1; $second <= $time; ++$second ) {
    $maxIdx = [];
    $max = 0;

    foreach ($deers as $deerIdx => $deer) {
        $distance = distance($deer, $second);

        if ($distance > $max) {
            $max = $distance;
            $maxIdx = [$deerIdx];
        } elseif ($distance === $max) {
            $maxIdx[] = $deerIdx;
        }
    }

    foreach ($maxIdx as $idx) {
        ++$deers[$idx]['points'];
    }
}

$max = 0;

foreach ($deers as $deer) {
    $max = \max($max, $deer['points']);
}

function distance($deer, $dt)
{
    $total = $deer['up'] + $deer['down'];
    $times = \floor($dt / $total);
    $x = $total * $times;
    $remainder = $dt - $x;

    if ($remainder > $deer['up']) {
        $add = $deer['up'];
    } else {
        $add = $remainder;
    }

    return $times * $deer['speed'] * $deer['up'] + $add * $deer['speed'];
}

answer($max);
