<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$floor = 0;

for ($i = 0; \mb_strlen($fileContent) > $i; ++$i) {
    $char = $fileContent[$i];

    if ('(' === $char) {
        ++$floor;
    } elseif (')' === $char) {
        --$floor;
    }
}

answer($floor);
