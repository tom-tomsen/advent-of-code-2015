<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$lines = \explode(\PHP_EOL, \trim($fileContent));

$registers = [];
$done = [];

$total = 0;

while (\count($done) !== \count($lines)) {
    foreach ($lines as $i => $line) {
        if (!\array_key_exists($i, $done)) {
            $line = \trim($line);
            $io = \explode(' -> ', $line);
            $command = \explode(' ', $io[0]);

            if (1 === \count($command)) {
                // 123 -> x
                if (\is_numeric($command[0])) {
                    $a = (int) $command[0];
                } elseif (\array_key_exists($command[0], $registers)) {
                    $a = $registers[$command[0]];
                } else {
                    continue;
                }

                $registers[$io[1]] = $a;
                $done[$i] = $line;
            } elseif ('AND' === $command[1]) {
                // x AND y -> d
                if (\is_numeric($command[0])) {
                    $a = (int) $command[0];
                } elseif (\array_key_exists($command[0], $registers)) {
                    $a = $registers[$command[0]];
                } else {
                    continue;
                }

                if (\is_numeric($command[2])) {
                    $b = (int) $command[2];
                } elseif (\array_key_exists($command[2], $registers)) {
                    $b = $registers[$command[2]];
                } else {
                    continue;
                }

                $registers[$io[1]] = $a & $b;
                $done[$i] = $line;
            } elseif ('OR' === $command[1]) {
                // x OR y -> d
                if (\is_numeric($command[0])) {
                    $a = (int) $command[0];
                } elseif (\array_key_exists($command[0], $registers)) {
                    $a = $registers[$command[0]];
                } else {
                    continue;
                }

                if (\is_numeric($command[2])) {
                    $b = (int) $command[2];
                } elseif (\array_key_exists($command[2], $registers)) {
                    $b = $registers[$command[2]];
                } else {
                    continue;
                }

                $registers[$io[1]] = $a | $b;
                $done[$i] = $line;
            } elseif ('NOT' === $command[0]) {
                // NOT e -> f
                if (\is_numeric($command[1])) {
                    $a = (int) $command[1];
                } elseif (\array_key_exists($command[1], $registers)) {
                    $a = $registers[$command[1]];
                } else {
                    continue;
                }

                $registers[$io[1]] = ((~$a ^ 0b1111111111111111) * -1) - 1;
                $done[$i] = $line;
            } elseif ('LSHIFT' === $command[1]) {
                // x LSHIFT 2 -> q
                if (\is_numeric($command[0])) {
                    $a = (int) $command[0];
                } elseif (\array_key_exists($command[0], $registers)) {
                    $a = $registers[$command[0]];
                } else {
                    continue;
                }

                if (\is_numeric($command[2])) {
                    $b = (int) $command[2];
                } elseif (\array_key_exists($command[2], $registers)) {
                    $b = $registers[$command[2]];
                } else {
                    continue;
                }

                $registers[$io[1]] = $a << $b;
                $done[$i] = $line;
            } elseif ('RSHIFT' === $command[1]) {
                // x RSHIFT 2 -> q
                if (\is_numeric($command[0])) {
                    $a = (int) $command[0];
                } elseif (\array_key_exists($command[0], $registers)) {
                    $a = $registers[$command[0]];
                } else {
                    continue;
                }

                if (\is_numeric($command[2])) {
                    $b = (int) $command[2];
                } elseif (\array_key_exists($command[2], $registers)) {
                    $b = $registers[$command[2]];
                } else {
                    continue;
                }

                $registers[$io[1]] = $a >> $b;
                $done[$i] = $line;
            } else {
                throw new \RuntimeException("Unknown command '{$line}'");
            }
        }
    }
}

answer($registers['a']);
