<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$input = \trim($fileContent);

$matches = [];
$new_input = '';

for ($n = 0; 50 > $n; ++$n) {
    $chr = $input[0];
    $c = 1;
    $new_input = '';

    for ($i = 1; \mb_strlen($input) > $i; ++$i) {
        if ($input[$i] === $chr) {
            ++$c;
        } else {
            $new_input .= $c . $chr;

            $chr = $input[$i];
            $c = 1;
        }
    }
    $new_input .= $c . $chr;
    $input = $new_input;
}

answer(\mb_strlen($input));
