<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$ingredients = [];

foreach ($lines as $line) {
    [$name, $cap, $dur, $fla, $tex, $cal] = \sscanf($line, '%s capacity %d, durability %d, flavor %d, texture %d, calories %d');
    $ingredients[] = [
        'name' => $name,
        'cap' => $cap,
        'dur' => $dur,
        'fla' => $fla,
        'tex' => $tex,
        'cal' => $cal,
    ];
}

$start = \microtime(true);
$max = 0;

for ($a = 0; 100 - 2 > $a; ++$a) {
    for ($b = 0; 100 - $a - 1 > $b; ++$b) {
        for ($c = 0; 100 - $b - 1 > $c; ++$c) {
            $d = 100 - $a - $b - $c;

            $result =
                \max(0, $a * $ingredients[0]['cap'] + $b * $ingredients[1]['cap'] + $c * $ingredients[2]['cap'] + $d * $ingredients[3]['cap']) *
                \max(0, $a * $ingredients[0]['dur'] + $b * $ingredients[1]['dur'] + $c * $ingredients[2]['dur'] + $d * $ingredients[3]['dur']) *
                \max(0, $a * $ingredients[0]['fla'] + $b * $ingredients[1]['fla'] + $c * $ingredients[2]['fla'] + $d * $ingredients[3]['fla']) *
                \max(0, $a * $ingredients[0]['tex'] + $b * $ingredients[1]['tex'] + $c * $ingredients[2]['tex'] + $d * $ingredients[3]['tex']);

            if ($result > $max) {
                $max = $result;
            }
        }
    }
}

answer($max);

function score($n, $i1, $m, $i2)
{
    return
        \max(0, $n * $i1['cap'] + $m * $i2['cap']) *
        \max(0, $n * $i1['dur'] + $m * $i2['dur']) *
        \max(0, $n * $i1['fla'] + $m * $i2['fla']) *
        \max(0, $n * $i1['tex'] + $m * $i2['tex']);
}
