<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$salt = \trim($fileContent);

$i = 0;

do {
    $hash = \md5($salt . (++$i));
} while ('00000' !== \mb_substr($hash, 0, 5));

answer($i);
