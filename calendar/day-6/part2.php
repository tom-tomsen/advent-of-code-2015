<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$lines = \explode(\PHP_EOL, \trim($fileContent));

$total = 0;

$lights = [];

for ($i = 0; 1000 > $i; ++$i) {
    $lights[$i] = [];

    for ($j = 0; 1000 > $j; ++$j ) {
        $lights[$i][$j] = 0;
    }
}

$count = 0;

foreach ($lines as $line) {
    $parts = \explode(' ', $line);

    if ('turn' === $parts[0] && 'on' === $parts[1]) {
        [$a, $b] = \sscanf($parts[2], '%d,%d');
        [$x, $y] = \sscanf($parts[4], '%d,%d');

        for ($i = $a; $i <= $x; ++$i) {
            for ($j = $b; $j <= $y; ++$j) {
                ++$lights[$i][$j];
            }
        }
    } elseif ('turn' === $parts[0] && 'off' === $parts[1]) {
        [$a, $b] = \sscanf($parts[2], '%d,%d');
        [$x, $y] = \sscanf($parts[4], '%d,%d');

        for ($i = $a; $i <= $x; ++$i) {
            for ($j = $b; $j <= $y; ++$j) {
                $lights[$i][$j] = \max(0, --$lights[$i][$j]);
            }
        }
    } elseif ('toggle' === $parts[0]) {
        [$a, $b] = \sscanf($parts[1], '%d,%d');
        [$x, $y] = \sscanf($parts[3], '%d,%d');

        for ($i = $a; $i <= $x; ++$i) {
            for ($j = $b; $j <= $y; ++$j) {
                $lights[$i][$j] += 2;
            }
        }
    }
}

$c = 0;

for ($i = 0; 1000 > $i; ++$i ) {
    for ($j = 0; 1000 > $j; ++$j) {
        if ($lights[$i][$j]) {
            $c += $lights[$i][$j];
        }
    }
}

answer($c);
