<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, $fileContent);

$total = 0;

foreach ($lines as $line) {
    $line = \trim($line);

    [$l, $w, $h] = \sscanf($line, '%dx%dx%d');

    $sides = [$l, $w, $h];
    \sort($sides);

    $small1 = \array_shift($sides);
    $small2 = \array_shift($sides);

    $total += 2 * $small1 + 2 * $small2 + $l * $w * $h;
}

answer($total);
