<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, $fileContent);

$total = 0;

foreach ($lines as $line) {
    $line = \trim($line);

    [$l, $w, $h] = \sscanf($line, '%dx%dx%d');

    $side1 = $l * $w;
    $side2 = $l * $h;
    $side3 = $h * $w;

    $smallSide = \min($side1, $side2, $side3);
    $area = 2 * $l * $w + 2 * $w * $h + 2 * $h * $l;

    $total += $smallSide + $area;
}

answer($total);
