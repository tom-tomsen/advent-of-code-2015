<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$password = \trim($fileContent);

$chr = [];
$idx = [];

for ($i = \ord('a'), $j = 0; \ord('z') >= $i; ++$i) {
    if (\ord('i') === $i || \ord('o') === $i || 'l' === $i) {
        continue;
    }

    $chr[\chr($i)] = $j;
    $idx[$j] = \chr($i);
    ++$j;
}

$straigt = [];

for ($i = \ord('a'); \ord('x') >= $i; ++$i) {
    $straigt[] = \chr($i) . \chr($i + 1) . \chr($i + 2);
}
$straigt_reg = \implode('|', $straigt);

$found = false;

while (!$found) {
    $password = \next($password, $chr, $idx);

    if (
        0 < \preg_match('~' . $straigt_reg . '~', $password) &&
        0 < \preg_match('~(.)\1.*(.)\2~', $password)
    ) {
        $found = true;
    }
}

answer($password);

function next($str, $chr, $idx)
{
    $i = \mb_strlen($str) - 1;
    $x = 1;
    $v = 0;

    while ($x > $v && 0 < $i) {
        $x = $chr[$str[$i]];
        $v = ++$x % \count($chr);
        $c = $idx[$v];

        $str[$i] = $c;
        --$i;
    }

    return $str;
}
