<?php

declare(strict_types=1);

// 1,1

// 1,2
// 2,1

// 1,3
// 2,2
// 3,1

namespace tomtomsen\AdventOfCode2015;

\error_reporting(-1);

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$i = 20151125;
$prev = $i;

for ($sRow = 1; 10000 > $sRow; ++$sRow ) {
    for ($c = 1, $r = $sRow; $c <= $sRow; $c++, $r--) {
        if (3010 === $r && 3019 === $c) {
            answer($i);
        }
        // var_dump(json_encode([$c, $r, $i]));
        $i = (($i * 252533) % 33554393);
    }
}
