<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2015;

function answer($answer) : void
{
    echo \PHP_EOL;
    echo 'ANSWER:' . \PHP_EOL;
    echo $answer;
    echo \PHP_EOL;
    echo \PHP_EOL;

    exit(0);
}
