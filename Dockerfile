FROM php:7-cli-alpine

RUN apk add --no-cache \
    curl \
    wget \
    unzip \
    zip \
    git \
    bash \
    ncurses \
    gnupg
