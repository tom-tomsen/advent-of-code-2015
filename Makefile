up:: build
	docker run --rm -ti -v ${PWD}:${PWD} -w ${PWD} tomtomsen/advent-of-code-2015:latest /bin/bash

build::
	docker build --tag tomtomsen/advent-of-code-2015:latest .
